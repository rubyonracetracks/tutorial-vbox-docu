---
id: chapter07
title: "Chapter 7: Installing SparkyLinux"
---

* Boot up your virtual machine with Sparky Linux, just as you did in the previous chapter.
* Double-click on the "Sparky Installer" icon.  (Use the regular installer, not the advanced installer or online installer.)
* When prompted, select your language, region, time zone, and keyboard.
* Select your disk partition configuration:
  * When you are in the Partitions section, select the manual partitioning option.
  * Click on the "New Partition Table" button.  
  * When you are prompted on the kind of partition you wish to create, select "Master Boot Record".  Select Free Space -> Create and stick with the default settings.  (If you had used the "Erase Disk" option, a significant portion of your disk space would be occupied by the swap partition.)
  * Click on "Next" to move on.
* Enter your name, username, name of computer, and password.  (You may use "rubyonracetracks" for the computer name.)  Toggle off the option to log in automatically.  Click on "Next" to continue.
* Check the parameters in the summary section.  If they are correct, click on "Next" to continue.
* The installation process will now begin and take several minutes to complete.  How long this takes depends on the speed of your computer.
* After the installation process is completed, click on "Done".
* Click on Menu -> Logout and select "Shutdown".
* The next chapter explains how to remove the ISO file from the virtual optical drive.  This is the virtual equivalent of ejecting the DVD from a physical optical drive.
