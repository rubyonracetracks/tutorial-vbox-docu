---
id: chapter01
title: "Chapter 1: Install VirtualBox"
sidebar_label: "Chapter 1: Install VirtualBox"
---

## Windows Users
* Go to the [VirtualBox](https://www.virtualbox.org) web site.
* Click on "Downloads".
* Click on "Windows hosts" to download the VirtualBox software.  
* After the download is complete, the installer should automatically run.  Follow its instructions.

## Mac Users
* Go to the [VirtualBox](https://www.virtualbox.org) web site.
* Click on "Downloads".
* Click on "OS X hosts" to download the VirtualBox software.
* After the download is complete, run this *.dmg file to start the installer.  Follow its instructions.

## Linux Users
* One method is to use your Linux distro's package management software to download and install VirtualBox.
* Another method is to go to the [VirtualBox](https://www.virtualbox.org) web site, click on "Downloads", and click on "Linux distributions" for further instructions.
