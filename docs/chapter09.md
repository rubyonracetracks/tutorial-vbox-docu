---
id: chapter09
title: "Chapter 9: Saving a Snapshot"
---

* From the VirtualBox main screen, select "SparkyLinux" and click on "Machine Tools".  Scroll down to the "Snapshots" section.
* Click on "Take" to take a snapshot of the current state.  Use the name "Initial installation" for this snapshot.
* Now you have saved the initial post-installation state of your SparkyLinux virtual machine.  If you ever want to or need to return your virtual machine to this state, you can restore this snapshot rather than reinstall SparkyLinux.
