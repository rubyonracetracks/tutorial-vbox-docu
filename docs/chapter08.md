---
id: chapter08
title: "Chapter 8: Emptying the Virtual Optical Drive"
---

* In the VirtualBox main screen, select "SparkyLinux", click on "Settings", and click on "Storage".  
* Select the SparkyLinux ISO file (under "Controller: IDE"), click on the disc symbol, and select the "Remove Disk from Virtual Drive" option.  The virtual IDE controller should now be empty.
* Click on "OK" to return to the VirtualBox main screen.
* The next chapter explains how to save a snapshot of the current state of your virtual machine.
