---
id: chapter06
title: "Chapter 6: Booting Up the Virtual Machine"
---

* In the main screen of VirtualBox, select "SparkyLinux" and click on "Start". This begins the process of booting up SparkyLinux.
* In the VirtualBox window, select View -> Full-screen Mode. This gives you the full-screen view of SparkyLinux.
* Read the dialog boxes that appear, and then close them. In a few minutes, you will be in SparkyLinux.
* If you ever wish to exit full-screen mode, just press the right Ctrl key and "f" key simultaneously.
* If you ever wish to access the VirtualBox menu when you're in full-screen mode, just press the right Ctrl key and the "Home" key simultaneously.
* Once SparkyLinux has booted up, you can browse the web from it, use LibreOffice, and do many other things you probably routinely do on your host machine. Keep in mind that because you are in live DVD mode, files you save to the virtual machine are NOT saved upon rebooting.
* To shut down the virtual machine, go to Menu -> Logout and click on "Shutdown".
